'use strict';

const express = require('express');
const cors = require('cors');
const debug = require('debug')('index.js');
const axios = require('axios');

function setUpExpressApp() {
  const app = express();
  app.use(express.urlencoded({
    extended: true
  }));
  app.use(express.static('static'));
  app.use(cors());
  return app;
}

function errorHandlerExpress(err, req, res, next) {
  console.error("ERROR, IP " + req.connection.remoteAddress);
  console.error(err.stack);
  res.status(500).send('Internal server error.');
}

let route = [
  "172.104.157.92",
  "176.58.101.61",
  "45.79.188.241",
  "173.255.220.98",
  "172.104.127.236",
  "192.53.174.92",
  "172.105.252.138",
  "172.104.157.92",  // Same as the first one to close the loop.
];
let known_keys = new Set();
let message_log = [];

function showMessagesHandler(req, res, next) {
  let result = '<html><head><title>Message Log</title></head><body>';
  for (let message of message_log) {
    result += message;
    result += '<br><hr>'
  }
  result += '</body></html>';
  res.send(result);
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function waitKeyHandler(req, res, next) {
  while (!known_keys.has(req.query.key)) {
    await sleep(10);
  }
  res.send('ok');
}

function sendTo(next_hop_index, key, message) {
  let next_hop_ip = route[next_hop_index];
  debug('Sending to ' + next_hop_ip)
  let encoded_message = encodeURIComponent(message);
  let next_hop_request = `http://${next_hop_ip}:8080/send_message?key=${key}&index=${next_hop_index}&message=${encoded_message}`;
  axios.get(next_hop_request)
    .then(function (response) {
      debug(response.data);
    })
    .catch(function (error) {
      debug(error);
    });
}

function sendMessageHandler(req, res, next) {
  // Extract index from request.
  let index = parseInt(req.query.index);
  debug('index = ' + index);

  // Extract key from request.
  let key = req.query.key;
  debug('key = ' + key);

  // Extract message from request.
  let message = req.query.message;
  debug('message = ' + message);

  // Save key.
  known_keys.add(`${key}_${index}`);
  message_log.push(message);

  // Compute next hop.
  let next_hop_index = index + 1;
  debug('next hop index is ' + next_hop_index)

  // Stop if we're at the end of the route.
  if (next_hop_index == route.length) {
    debug('i am final hop');
    res.send('ok');
    return;
  }

  // Send to next hop, using timeout.
  setTimeout(function () {
    sendTo(next_hop_index, key, message);
  }, 100);

  res.send('ok');
}

function main() {
  const express_app = setUpExpressApp();

  express_app.get('/send_message', sendMessageHandler);
  express_app.get('/wait_key', waitKeyHandler);
  express_app.get('/messages', showMessagesHandler);
  express_app.use(errorHandlerExpress);

  let express_app_port = 8080;
  express_app.listen(express_app_port, () => {
    debug(`Express server listening on port ${express_app_port}.`);
  });
  debug('Initialized.');
}

main();